from django.contrib import admin
from django.contrib.admin import site

from ivtsite.models import Postmovie, Feedback

admin.site.register(Postmovie)

site.register(Feedback)
