# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-07 05:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ivtsite', '0002_post'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='cread_date',
            new_name='create_date',
        ),
        migrations.AlterField(
            model_name='post',
            name='text',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.CharField(max_length=1000),
        ),
    ]
