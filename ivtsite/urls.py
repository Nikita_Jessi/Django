"""ivtsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from ivtsite import views
from ivtsite.views import HomeView, AnimeView, SeriesView, ProfilePage, RegisterView, logout, FeedbackView, \
    ViewFeedbacksView, Postmovie, GetMarksAjaxView
from ivtsite.views import FilmsView

urlpatterns = [
                  url(r'^feedback/$', FeedbackView.as_view(), name='feedback'),
                  url(r'^get-marks/$', GetMarksAjaxView.as_view()),
                  url(r'^view_feedback/$', ViewFeedbacksView.as_view(), name='view_feedback'),
                  url(r'^post/$', Postmovie.as_view(), name="Post"),
                  url(r'^accounts/register/$', RegisterView.as_view(), name="register"),
                  url(r'^accounts/login/$', login, name="login"),
                  url(r'^accounts/logout/$', logout, name="logout"),
                  url(r'^accounts/profile/$', ProfilePage.as_view(), name="profile"),

                  url(r'^$', HomeView.as_view(), name="home"),
                  url(r'^admin/', admin.site.urls),
                  url(r'^films/$', FilmsView.as_view(), name='films'),
                  url(r'^anime/$', AnimeView.as_view(), name='anime'),
                  url(r'^series/$', SeriesView.as_view(), name='series'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
              + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
